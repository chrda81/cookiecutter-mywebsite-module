# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class {{ cookiecutter.project_name|replace("mywebsite_","")|capitalize }}Config(AppConfig):
    """The default AppConfig for the website module ``{{ cookiecutter.project_name }}``."""
    name = '{{ cookiecutter.project_name }}'
    verbose_name = _("{{ cookiecutter.project_name|replace("mywebsite_","")|capitalize }}")
