# CookieCutter-MyWebsite-Module

Cookiecutter template for MyWebsite module. This helps creating a module for the customizable WebApp based on the mywebsite-base-django package.

***
License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under http://www.d-fsl.org.
***

If you like my work, I would appreciate a donation. You can find several options on my website www.dsoft-app-dev.de at section crowdfunding. Thank you!


## Installation

	$ pip install cookiecutter
	$ cookiecutter CookieCutter-MyWebsite-Module

or

	$ cookiecutter https://bitbucket.org/chrda81/cookiecutter-mywebsite-module.git
